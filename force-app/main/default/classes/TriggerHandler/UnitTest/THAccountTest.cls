@isTest
public class THAccountTest {

    @TestSetup
    static void makeData(){        
        List<User> userList = new List<User>{
            (User) DFUser.getInstance().getSingle('System Administrator'),
            (User) DFUser.getInstance().getSingle('Standard User')
        };
        insert userList;
        System.debug(LoggingLevel.DEBUG, userList);
    }
    
    @IsTest
    static void methodName(){
        User user = [SELECT Id FROM User WHERE Username = 'sysadmin1@test.notvalid'];
        Account account = (Account) DFAccount.getInstance().getSingle('BusinessAccount');
        Test.startTest();
        System.runAs(user){
            insert account;
        }
        Test.stopTest();
        Account updatedAccount = [
            SELECT Id, AccountSource
            FROM Account
            WHERE Id = :account.Id
        ];
        Assert.areEqual('Other', updatedAccount.AccountSource, 'AccountSource is empty or not correct');
    }

}