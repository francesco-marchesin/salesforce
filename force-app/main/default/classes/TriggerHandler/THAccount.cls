public with sharing class THAccount extends TriggerHandler {

    private static THAccount instance;
    private static User currentUser;

    private THAccount(){
        currentUser = this.getCurrentUser();
    }

    public static THAccount getInstance(){
        if(null == instance){
            instance = new THAccount();
        }
        return instance;
    }

    protected override void onBeforeInsert(){
        for(Account account : (List<Account>) this.newList){
            account.AccountSource = String.isBlank(account.AccountSource) ? 'Other' : account.AccountSource;
        }
    }

}