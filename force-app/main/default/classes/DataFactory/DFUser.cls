@isTest
public class DFUser extends DataFactory {

    private static DFUser instance;

    private DFUser(){
        this.addBlueprint(
            new DataFactory.Blueprint(
                new User(
                    LastName = 'SysAdmin{0}',
                    Email = 'sysadmin{0}@test.notvalid',
                    Alias = 'syad{0}',
                    Username = 'sysadmin{0}@test.notvalid',
                    CommunityNickname = 'sysadmin{0}',
                    LocaleSidKey = 'en_US',
                    TimeZoneSidKey = 'GMT',
                    Profile = new Profile(Name = 'System Administrator'),
                    LanguageLocaleKey = 'en_US',
                    EmailEncodingKey = 'UTF-8'
                )
            )
            .setClassType(User.class)
            .setAsDefault()
            .setKeyOrRecordTypeDeveloperName('System Administrator')
        );
        this.addBlueprint(
            new DataFactory.Blueprint(
                new User(
                    LastName = 'StandardUser{0}',
                    Email = 'standarduser{0}@test.notvalid',
                    Alias = 'stus{0}',
                    Username = 'standarduser{0}@test.notvalid',
                    CommunityNickname = 'standarduser{0}',
                    LocaleSidKey = 'en_US',
                    TimeZoneSidKey = 'GMT',
                    Profile = new Profile(Name = 'Standard User'),
                    LanguageLocaleKey = 'en_US',
                    EmailEncodingKey = 'UTF-8'
                )
            )
            .setClassType(User.class)
            .setKeyOrRecordTypeDeveloperName('Standard User')
        );
    }

    public static DFUser getInstance(){
        if(null == instance){
            instance = new DFUser();
        }
        return instance;
    }

}