@isTest
public class DFAccount extends DataFactory {

    private static DFAccount instance;

    private DFAccount(){
        this.addBlueprint(
            new DataFactory.Blueprint(
                new Account(
                    Name = 'Company {0}'
                )
            )
            .setClassType(Account.class)
            .setAsDefault()
            .setKeyOrRecordTypeDeveloperName('BusinessAccount')
        );
        this.addBlueprint(
            new DataFactory.Blueprint(
                new Account(
                    LastName = 'Surname {0}'
                )
            )
            .setClassType(Account.class)
            .setKeyOrRecordTypeDeveloperName('PersonAccount')
        );
    }

    public static DFAccount getInstance(){
        if(null == instance){
            instance = new DFAccount();
        }
        return instance;
    }

}