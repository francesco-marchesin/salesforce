public virtual class TriggerHandler {

    protected Boolean isExecuting = false;
    protected List<SObject> newList = new List<SObject>();
    protected Map<Id, SObject> newMap = new Map<Id, SObject>();
    protected List<SObject> oldList = new List<SObject>();
    protected Map<Id, SObject> oldMap = new Map<Id, SObject>();
    protected TriggerOperation operationType;
    protected Integer size = 0;
    protected Type classType;
    protected String customPermission;

    private static Set<Type> disabledTriggerHandlerSet = new Set<Type>();

    public static void disable(Type classType){
        disabledTriggerHandlerSet.add(classType);
    }

    public static void enable(Type classType){
        if(disabledTriggerHandlerSet.contains(classType)){
            disabledTriggerHandlerSet.remove(classType);
        }
    }

    public TriggerHandler setIsExecuting(Boolean isExecuting){
        this.isExecuting = isExecuting;
        return this;
    }

    public TriggerHandler setNewList(List<SObject> newList){
        this.newList = newList;
        return this;
    }

    public TriggerHandler setNewMap(Map<Id, SObject> newMap){
        this.newMap = newMap;
        return this;
    }

    public TriggerHandler setOldList(List<SObject> oldList){
        this.oldList = oldList;
        return this;
    }

    public TriggerHandler setOldMap(Map<Id, SObject> oldMap){
        this.oldMap = oldMap;
        return this;
    }

    public TriggerHandler setOperationType(TriggerOperation operationType){
        this.operationType = operationType;
        return this;
    }

    public TriggerHandler setSize(Integer size){
        this.size = size;
        return this;
    }

    public TriggerHandler setClassType(Type classType){
        this.classType = classType;
        return this;
    }

    public TriggerHandler setCustomPermission(String customPermission){
        this.customPermission = customPermission;
        return this;
    }

    public void run(){
        if(this.isDisabled()){
            return;
        }
        switch on this.operationType {
            when BEFORE_INSERT{
                this.onBeforeInsert();
            }
            when AFTER_INSERT{
                this.onAfterInsert();
            }
            when BEFORE_UPDATE{
                this.onBeforeUpdate();
            }
            when AFTER_UPDATE{
                this.onAfterUpdate();
            }
            when BEFORE_DELETE{
                this.onBeforeDelete();
            }
            when AFTER_DELETE{
                this.onAfterDelete();
            }
            when AFTER_UNDELETE{
                this.onAfterUndelete();
            }
        }
    }

    protected virtual void onBeforeInsert(){} // NOPMD
    protected virtual void onAfterInsert(){} // NOPMD
    protected virtual void onBeforeUpdate(){} // NOPMD
    protected virtual void onAfterUpdate(){} // NOPMD
    protected virtual void onBeforeDelete(){} // NOPMD
    protected virtual void onAfterDelete(){} // NOPMD
    protected virtual void onAfterUndelete(){} // NOPMD

    protected Boolean isDisabled(){
        return disabledTriggerHandlerSet.contains(this.classType) || (!String.isBlank(this.customPermission) && FeatureManagement.checkPermission(this.customPermission));
    }

    protected User getCurrentUser(){
        return [
            SELECT Id, Name, Email, Username, ProfileId, UserRoleId, Profile.Name, UserRole.Name
            FROM User
            WHERE Id = :UserInfo.getUserId()
        ];
    }

}