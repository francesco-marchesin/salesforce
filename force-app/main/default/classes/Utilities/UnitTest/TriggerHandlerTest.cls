@isTest
private class TriggerHandlerTest {

    @isTest
    static void testBeforeInsert() {
        TriggerHandler handler = getTriggerHandler();
        handler.run();
    }

    @isTest
    static void testAfterInsert() {
        TriggerHandler handler = getTriggerHandler();
        handler.setOperationType(TriggerOperation.AFTER_INSERT);
        handler.run();
    }

    @isTest
    static void testBeforeUpdate() {
        TriggerHandler handler = getTriggerHandler();
        handler.setOperationType(TriggerOperation.BEFORE_UPDATE);
        handler.run();
    }

    @isTest
    static void testAfterUpdate() {
        TriggerHandler handler = getTriggerHandler();
        handler.setOperationType(TriggerOperation.AFTER_UPDATE);
        handler.run();
    }

    @isTest
    static void testBeforeDelete() {
        TriggerHandler handler = getTriggerHandler();
        handler.setOperationType(TriggerOperation.BEFORE_DELETE);
        handler.run();
    }

    @isTest
    static void testAfterDelete() {
        TriggerHandler handler = getTriggerHandler();
        handler.setOperationType(TriggerOperation.AFTER_DELETE);
        handler.run();
    }

    @isTest
    static void testAfterUndelete() {
        TriggerHandler handler = getTriggerHandler();
        handler.setOperationType(TriggerOperation.AFTER_UNDELETE);
        handler.run();
    }

    @isTest
    static void testBeforeInsertDisabledWrongClass() {
        TriggerHandler handler = getTriggerHandler();
        TriggerHandler.disable(EncodingUtil.class);
        handler.run();
        TriggerHandler.enable(EncodingUtil.class);
    }

    @isTest
    static void testBeforeInsertDisabledRightClass() {
        TriggerHandler handler = getTriggerHandler();
        TriggerHandler.disable(TriggerHandler.class);
        handler.run();
        TriggerHandler.enable(TriggerHandler.class);
    }

    static TriggerHandler getTriggerHandler(){
        return new TriggerHandler()
            .setIsExecuting(true)
            .setNewList(new List<Account>())
            .setNewMap(new Map<Id, Account>())
            .setOldList(new List<Account>())
            .setOldMap(new Map<Id, Account>())
            .setOperationType(TriggerOperation.BEFORE_INSERT)
            .setSize(0)
            .setClassType(TriggerHandler.class)
            .setCustomPermission('SkipTriggerHandler');
    }

}