public virtual class DataFactory {

    private static final String DEFAULT_RECORD_TYPE_DEVELOPER_NAME = '_DEFAULT';

    private Map<String, Blueprint> blueprintByKeyMap = new Map<String, Blueprint>();

    public List<SObject> getMultiple(Integer numberOfSObjects, String keyOrRecordTypeDeveloperName){
        List<SObject> sObjectList = new List<SObject>();
        if(null == keyOrRecordTypeDeveloperName){
            keyOrRecordTypeDeveloperName = DEFAULT_RECORD_TYPE_DEVELOPER_NAME;
        }
        for(Integer i = 0; i < numberOfSObjects; i++){
            if(!blueprintByKeyMap.containsKey(keyOrRecordTypeDeveloperName)){
                throw new BlueprintNotFoundException();
            }
            sObjectList.add(blueprintByKeyMap.get(keyOrRecordTypeDeveloperName).getOne());
        }
        return sObjectList;
    }
    
    public List<SObject> getMultiple(Integer numberOfSObjects){
        return this.getMultiple(numberOfSObjects, DEFAULT_RECORD_TYPE_DEVELOPER_NAME);
    }
    
    public SObject getSingle(String keyOrRecordTypeDeveloperName){
        return this.getMultiple(1, keyOrRecordTypeDeveloperName)[0];
    }

    public SObject getSingle(){
        return this.getMultiple(1, DEFAULT_RECORD_TYPE_DEVELOPER_NAME)[0];
    }

    protected void addBlueprint(Blueprint blueprint){
        if(!blueprint.hasKeyOrRecordTypeDeveloperName()){
            if(!this.blueprintByKeyMap.containsKey(DEFAULT_RECORD_TYPE_DEVELOPER_NAME)){
                blueprint.setAsDefault();
                this.blueprintByKeyMap.put(DEFAULT_RECORD_TYPE_DEVELOPER_NAME, blueprint);
            }else{
                throw new MissingKeyOrRecordTypeDeveloperNameException();
            }
        }else{
            this.blueprintByKeyMap.put(blueprint.getKeyOrRecordTypeDeveloperName(), blueprint);
            if(blueprint.isSetAsDefault()){
                if(!this.blueprintByKeyMap.containsKey(DEFAULT_RECORD_TYPE_DEVELOPER_NAME)){
                    this.blueprintByKeyMap.put(DEFAULT_RECORD_TYPE_DEVELOPER_NAME, blueprint);
                }else{
                    throw new DefaultBlueprintAlreadyExistsException();
                }
            }
        }
    }

    public class Blueprint{

        private SObject blueprint;
        private Type classType;
        private String className;
        private Integer counter = 1;
        private String keyOrRecordTypeDeveloperName;
        private Id recordTypeId;
        private Map<String, Object> valueByFieldMap;

        public Blueprint(SObject blueprint){
            this.blueprint = blueprint;
            this.valueByFieldMap = bluePrint.getPopulatedFieldsAsMap();
        }

        public Blueprint setClassType(Type classType){
            this.classType = classType;
            this.className = String.valueOf(classType).split(':')[0];
            return this;
        }

        public Blueprint setAsDefault(){
            this.keyOrRecordTypeDeveloperName = DEFAULT_RECORD_TYPE_DEVELOPER_NAME;
            return this;
        }

        public Boolean isSetAsDefault(){
            return this.keyOrRecordTypeDeveloperName == DEFAULT_RECORD_TYPE_DEVELOPER_NAME;
        }

        public Boolean hasKeyOrRecordTypeDeveloperName(){
            return !String.isBlank(this.keyOrRecordTypeDeveloperName);
        }

        public Blueprint setKeyOrRecordTypeDeveloperName(String keyOrRecordTypeDeveloperName){
            this.keyOrRecordTypeDeveloperName = keyOrRecordTypeDeveloperName;
            this.setRecordTypeId();
            return this;
        }

        public String getKeyOrRecordTypeDeveloperName(){
            return this.keyOrRecordTypeDeveloperName;
        }

        public SObject getOne(){
            SObject newBluePrint = this.blueprint.clone();
            this.setBlueprintRecordTypeId(newBluePrint);
            this.setDynamicFields(newBluePrint);
            this.counter++;
            return newBluePrint;
        }

        private void setRecordTypeId(){
            if(null == this.keyOrRecordTypeDeveloperName || null == this.className){
                return;
            }
            Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe();
            if(!globalDescribeMap.containsKey(this.className)){
                return;
            }
            Map<String, Schema.RecordTypeInfo> recordTypeInfoMap = globalDescribeMap.get(this.className).getDescribe().getRecordTypeInfosByDeveloperName();
            if(!recordTypeInfoMap.containsKey(this.keyOrRecordTypeDeveloperName)){
                return;
            }
            this.recordTypeId = recordTypeInfoMap.get(this.keyOrRecordTypeDeveloperName).getRecordTypeId();
        }

        private void setBlueprintRecordTypeId(SObject blueprint){
            if(null != this.recordTypeId){
                blueprint.put('RecordTypeId', this.recordTypeId);
            }
        }

        private void setDynamicFields(SObject bluePrint){
            for(String fieldName : this.valueByFieldMap.keySet()){
                if(this.valueByFieldMap.get(fieldName) instanceof String){
                    bluePrint.put(fieldName, String.format((String) bluePrint.get(fieldName), new List<String>{String.valueOf(this.counter)}));
                }
            }
        }
    }

    public class MissingKeyOrRecordTypeDeveloperNameException extends Exception{}
    public class DefaultBlueprintAlreadyExistsException extends Exception{}
    public class BlueprintNotFoundException extends Exception{}

}