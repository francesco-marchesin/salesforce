trigger AccountTrigger on Account (before insert) {
    
    THAccount.getInstance()
        .setIsExecuting(Trigger.isExecuting)
        .setNewList(Trigger.new)
        .setNewMap(Trigger.newMap)
        .setOldList(Trigger.old)
        .setOldMap(Trigger.oldMap)
        .setOperationType(Trigger.operationType)
        .setSize(Trigger.size)
        .setClassType(THAccount.class)
        .setCustomPermission('SkipTHAccount')
        .run();

}